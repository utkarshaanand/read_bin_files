import glob
import json
import utils

binFilesDirectory = './binFiles'
fileNameList = glob.glob(binFilesDirectory+'/*.bin')
jsonDict = {}

for fileName in fileNameList:
    data,buf = utils.read_binary(fileName)
    
    keyName = utils.get_fileName(fileName)
    jsonDict[keyName] = []
    for d in data:
        jsonDict[keyName].append(d)
        
utils.save_jsonFile(jsonDict,'binFiles.json')
