import struct
import platform
import json

############################################################
# READ DIFFERENT TYPES OF BINARY FILES LOCATED IN THE
# /RAP_FS/CFG DIRECTORY. DIFFERENT STYLE OF UNPACKING IS
# USED BASED ON THE FILE NAME.
def read_binary(inputFile):
    with open(inputFile,mode='rb') as f:
        buf = f.read()
    if (('calibrationMatrix' in inputFile) or ('CalibrationMatrix' in inputFile)):
        data = struct.unpack('9f',buf)
    elif ('cam_intrinsic' in inputFile):
        data = struct.unpack('14d',buf)
    elif (('distortionCoeff' in inputFile) or ('DistortionCoeff' in inputFile)):
        data = struct.unpack('5f',buf)
    elif (('fieldCalibRotation' in inputFile)):
        data = struct.unpack('9f',buf)
    elif ('fieldCalibTranslation' in inputFile):
        data = struct.unpack('3f',buf)
    elif ('step2CalibF1HB3Dpoints' in inputFile):
        data = struct.unpack('30f',buf)
    elif ('step2Calib' in inputFile):
        data = struct.unpack('24f',buf)
    else:
        data = buf
    return data,buf
############################################################

############################################################
# GENERATE THE FILE NAME FROM THE FULL FILE PATH. THIS NAME
# IS USED TO CREATE AN APPROPRIATE KEY IN THE PYTHON
# DICTIONARY WHICH IS LATER SAVED AS A JSON FILE.
def get_fileName(inputFile):
    if (platform.system()=='Windows'):
        fileName = inputFile.split('\\')[-1]
        fileName = fileName.split('.bin')[0]
    elif (platform.system()=='Linux'):
        fileName = inputFile.split('/')[-1]
        fileName = fileName.split('.bin')[0]
    return fileName
############################################################

############################################################
# SAVING THE PYTHON DICTIONARY AS A JSON FILE
def save_jsonFile(jsonDict,jsonFileName):
    outFile = open(jsonFileName,'w')
    json.dump(jsonDict,outFile,indent=4)
    outFile.close()
############################################################
